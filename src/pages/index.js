import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

import "./index.css"
import logo from "../images/hacker-day-logo.svg"
import hasp from "../images/hasp.png"
import foss from "../images/foss.png"
import edc from "../images/edc.png"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <div className="index">
      <div className="intro">
        <div className="logo">
          <img src={logo} alt="HackerDay" className="hacker-day-logo"/>
          <span className="date">
            27<sup>th</sup>&nbsp;&nbsp;February&nbsp;&nbsp;2020
          </span>
          <a href="https://www.yepdesk.com/hasp-hacker-day" className="register">Register Now</a>
        </div>
        <div className="about">
          <p className="about-para">
            Hacker Day is an immersive hackathon where students with an interest in technology gather together to build a single project, a website for Hacker Space NSSCE
          </p>
        </div>
      </div>
      <div className="venue">
        <h4 className="venue-title">Venue & Time</h4>
        <h1>NSS College of Engineering</h1>
        <h3>IOT LAB</h3>
        <h3>09:00 AM Onwards</h3>
        <a className="schedule-btn" href="https://www.notion.so/06c39bb7049842199875b366e36a5a29?v=04014408d9474fe997e7a99a4f94fa0e">Schedule</a>      
      </div>
      <div className="faq">
        <h2>FAQ</h2>
        <div>
          <h3 className="question">
            Is Hacker Day Free?
          </h3>
          <h3 className="answer">
            Yep! Hacker Day is Totally Free And Always Will Be!
          </h3>
        </div>
        <div>
          <h3 className="question">
            Who all can participate?
          </h3>
          <h3 className="answer">
            This Hackathon is exclusively for NSSCE students. Anyone from NSSCE can participate.
          </h3>
        </div>
        <div>
          <h3 className="question">
            Do I have to be qualified?
          </h3>
          <h3 className="answer">
            If you love to code, you are more than welcome to participate in the Hackathon.
          </h3>
        </div>
      </div>
      <div className="contact">
        <h1>Contact</h1>
        <h5>Allen - 8137073386</h5>
        <h5>Abhinav - 8921662654</h5>
        <h5>Email - hasp.nssce@gmail.com</h5>
      </div>
      <div className="sponsors">
        <img src={hasp} alt="HASP" />
        <img src={foss} alt="FOSS" />
        <img src={edc} alt="EDC" />
      </div>
      <footer>
        Copyright &copy; 2020 <a href="https://hasp-nssce.gitlab.io/hasp-website/">&nbsp;&nbsp;Hacker Space NSSCE</a>. All Rights Reserved
      </footer>
    </div>
  </Layout>
)

export default IndexPage
